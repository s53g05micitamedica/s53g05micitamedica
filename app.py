from flask import Flask, render_template, request, redirect, url_for, flash
from flask.globals import request
from flask_mysqldb import MySQL

app = Flask(__name__)
app.config["MYSQL_HOSTH"] = "localhost"
app.config["MYSQL_USER"] = "root"
app.config["MYSQL_PASSWORD"] = ""
app.config["MYSQL_DB"] = "micitamedica"

mysql = MySQL(app)

app.secret_key = "mysecretkey"

@app.route("/")
def Index():
    return render_template('index.html')


@app.route("/cita")
def Cita():
    cursor = mysql.connection.cursor()
    cursor.execute("SELECT * FROM especialidades")
    data = cursor.fetchall()
    return render_template('cita.html',especialidades = data)

@app.route("/cita1/<especialidad>")
def Cita1(especialidad):
    print(especialidad)
    cursor = mysql.connection.cursor()
    cursor.execute("SELECT * FROM medicos WHERE especialidad=%s",[especialidad])
    data = cursor.fetchall()
    return render_template('cita1.html',medicos = data,especialidad=especialidad)

@app.route("/cita2/<medico>/<especialidad>")
def Cita2(medico=None,especialidad=None):
    print(medico)
    print(especialidad)
    cursor = mysql.connection.cursor()
    cursor.execute("SELECT * FROM citas WHERE medico=%s and especialidad=%s and estado=%s",
    [medico,especialidad,"0"])
    data = cursor.fetchall()
    return render_template('cita2.html',citas = data,medico=medico,especialidad=especialidad)

@app.route("/cita3/<dia>/<hora>/<medico>/<especialidad>")
def Cita3(dia=None,hora=None,medico=None,especialidad=None):
    print("entra")
    print(dia)
    print(hora)
    print(medico)
    print(especialidad)
    
    return render_template('cita3.html',dia=dia, hora=hora,
    medico=medico,especialidad=especialidad)

@app.route("/update/<dia>/<hora>/<medico>/<especialidad>",methods=["POST"])
def update_contact(dia=None,hora=None,medico=None,especialidad=None):
    if request.method == "POST":
        nombre = request.form["nombre"]
        cedula = request.form["cedula"]
        telefono = request.form["telefono"]
        email = request.form["email"]
        mensaje = request.form["mensaje"]
        print(nombre,cedula,telefono,email,mensaje,"1",dia,hora,medico,especialidad)
        cursor = mysql.connection.cursor()
        cursor.execute("""
        UPDATE citas 
        SET usuario = %s,
            cedula = %s,
            telefono = %s,
            correo = %s,
            mensaje = %s,
            estado = %s
        WHERE dia =%s and hora =%s and medico=%s and especialidad=%s and estado=%s
        """,[nombre,cedula,telefono,email,mensaje,"1",dia,hora,medico,especialidad,"0"])
        mysql.connection.commit()
        flash("Contacto Actualizado Satisfactoriamente")
        return redirect(url_for("Index"))

@app.route("/contacto")
def Contacto():
    return render_template('contacto.html')


@app.route("/especialidades")
def Especialidades():
    return render_template('especialidades.html')

@app.route("/medicos")
def Medicos():
    return render_template('medicos.html')

@app.route("/nosotros")
def Nosotros():
    return render_template('nosotros.html')

@app.route("/plataforma")
def Plataforma():
    return render_template('plataforma.html')

@app.route("/admin")
def Admin():
    cursor = mysql.connection.cursor()
    cursor.execute("SELECT * FROM especialidades order by especialidades")
    data = cursor.fetchall()

    cursor.execute("SELECT * FROM medicos order by especialidad")
    data1 = cursor.fetchall()

    cursor.execute("SELECT * FROM hors")
    data2 = cursor.fetchall()

    cursor.execute("SELECT * FROM citas where estado=%s order by medico",["0"])
    data3 = cursor.fetchall()
    

    return render_template('admin.html',especialidades=data,medicos=data1,horas=data2,citas=data3)

@app.route("/add_esp",methods=["POST"])
def add_esp():
    if request.method == "POST":
        especialidad = request.form["especialidad"]
        cursor = mysql.connection.cursor()
        cursor.execute("INSERT into especialidades (especialidades) VALUES (%s)",
        [especialidad])

        mysql.connection.commit()
        flash("Especialidad Agregada Satisfactoriamente")
        return redirect(url_for("Admin"))


@app.route("/delete_esp/<string:id>")
def delete_esp(id):
    cursor = mysql.connection.cursor()
    cursor.execute("DELETE FROM especialidades WHERE id = {0}".format(id))
    mysql.connection.commit()
    flash("Especialidad Eliminada Satisfactoriamente")
    return redirect(url_for("Admin"))

@app.route("/add_med",methods=["POST"])
def add_med():
    if request.method == "POST":
        medico = request.form["medico_med"]
        especialidad = request.form["esp_med"]
        cursor = mysql.connection.cursor()
        cursor.execute("INSERT into medicos (nombre,especialidad) VALUES (%s,%s)",
        [medico,especialidad])

        mysql.connection.commit()
        flash("Medico Agregada Satisfactoriamente")
        return redirect(url_for("Admin"))


@app.route("/delete_med/<string:id>")
def delete_med(id):
    cursor = mysql.connection.cursor()
    cursor.execute("DELETE FROM medicos WHERE id = {0}".format(id))
    mysql.connection.commit()
    flash("Especialidad Eliminada Satisfactoriamente")
    return redirect(url_for("Admin"))

@app.route("/add_cita",methods=["POST"])
def add_cita():
    if request.method == "POST":
        
        dato_med = request.form["esp_cita"]
        dato_med = dato_med.split("-")
        especialidad = dato_med[0]
        medico = dato_med[1]
        print(especialidad)
        print(medico)
        dia = request.form["dia_cita"]
        hora = request.form["hora_cita"]
        cursor = mysql.connection.cursor()
        cursor.execute("INSERT into citas (medico,especialidad,dia,hora,estado) VALUES (%s,%s,%s,%s,%s)",
        [medico,especialidad,dia,hora,"0"])

        mysql.connection.commit()
        flash("Medico Agregada Satisfactoriamente")
        return redirect(url_for("Admin"))


@app.route("/delete_cita/<string:id>")
def delete_cita(id):
    cursor = mysql.connection.cursor()
    cursor.execute("DELETE FROM medicos WHERE id = {0}".format(id))
    mysql.connection.commit()
    flash("Especialidad Eliminada Satisfactoriamente")
    return redirect(url_for("Admin"))


if __name__ == "__main__":
    app.run(port=8000, debug=True)
